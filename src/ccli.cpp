							/* D�BUT DU PROGRAMME */
///////////////////////////////////////////////////////////////////////////////
// FILE:		AutoCCLI.CPP 
// AUTHOR:		Francis MARCOGLIESE 
// DATE:		8/9/2012 
// DESCRIPTION: This program logs in to CCLI and downloads a list of songs based
//				on a the CCLI numbers in a text document.
///////////////////////////////////////////////////////////////////////////////

#include <iostream>	// Pour l'utilisation de cin et cout
#include <string>	// Pour l'utilisation du type string
#include <fstream>	// Pour l'utilisation des fonctions de lecture de fichiers 
					//textes
#include <curl.h>	// To use cURL
#include <sstream>	// To use sstream

using namespace std;

size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata) {
    ostringstream *streamTitle = (ostringstream*)userdata;
    size_t count = size * nmemb;
    streamTitle->write(ptr, count);
    return count;
}

int main (void)
{
	
  CURL			*curl,
				*curl2,
				*curl3,
				*curl4,
				*curl5;
  CURLcode		res;
  ostringstream streamTitle;
  bool			criterion = true;
  int			i = 0;
  string		slash,
				output,
				title,
				URL,
				Post;
  char			*URLc,
				*Postc,
				*Tokenc;

  curl = curl_easy_init();
  if(curl)
  {
	
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &streamTitle);
	curl_easy_setopt(curl, CURLOPT_URL, "https://ca.songselect.com/songs/3108066/"); //correct to cstr
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    res = curl_easy_perform(curl);


	output = streamTitle.str();
	output.erase(0,98);
	while(criterion)
	{
		slash = output[i];
		if(slash == "/")
		{
			criterion = false;
		}
		else
			i++;
	}
	
	title = output.substr(0,i);
	
	// always cleanup 
    curl_easy_cleanup(curl);
  }
  cout << title;
  curl = curl_easy_init();
  if(curl)
  {
	URL = "https://ca.songselect.com/songs/3108066/" + title + "/lyrics/download";
	URLc = new char [URL.size()+1];
	strcpy (URLc, URL.c_str());
	curl_easy_setopt(curl, CURLOPT_URL, URLc);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    res = curl_easy_perform(curl);
	cout << res;
	// always cleanup 
    curl_easy_cleanup(curl);
  }
  
  curl3 = curl_easy_init();
  if(curl3)
  {

	curl_easy_setopt(curl3, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
	curl_easy_setopt(curl3, CURLOPT_URL, "https://ca.songselect.com/account/login?ReturnUrl=%2F");
	curl_easy_setopt(curl3, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl3, CURLOPT_WRITEFUNCTION, write_data);
	curl_easy_setopt(curl3, CURLOPT_WRITEDATA, &streamTitle);
	curl_easy_setopt(curl3, CURLOPT_REFERER, "https://ca.songselect.com");
	curl_easy_setopt(curl3, CURLOPT_COOKIEJAR, "cookie.txt");
    res = curl_easy_perform(curl3);
	// always cleanup 
    curl_easy_cleanup(curl3);
  }

  output.clear();
  output = streamTitle.str();
  cout << output;
  //////////////////////////
  //////////////////////////
  /*string	line,
			letterR,
			letterE,
			letterQ,
			letterU,
			letterE2,
			letterS,
			letterT,
			Token;
  int		j=0,
			position = 0;
  bool		continuer = true;
  
  for(int i=0; i<output.size(); i++)
  {
	  letterR = output[i];
	  letterE = output[i+1];
	  letterQ = output[i+2];
	  letterU = output[i+3];
	  letterE2 = output[i+4];
	  letterS = output[i+5];
	  letterT = output[i+6];
	  if(letterR == "R" & letterE == "e" & letterQ == "q" & letterU == "u" & letterE2 == "e" & letterS == "s" & letterT == "t")
	  {
		  position = i + 47;
		  break;
	  }
  }
   //cout << position;
	Token = output;
	Token.erase(0, position);
	//cout << Token;
	Token.erase(156, Token.size());
	//cout << Token;*/
  /////////////////////////////
  //////////////////////////
 string	line,
			letter1,
			letter2,
			Token;
  ifstream	cookie;
  int		j=0,
			position = 0;

  cookie.open("cookie.txt");
  for(int i=0; i<6; i++)
  {
	  getline(cookie, line, '\n');
  };
  
  for(int i=0; i<line.size(); i++)
  {
	  letter1 = line[i];
	  letter2 = line[i+1];
	  if(letter1 == "_" & letter2 == "_")
	  {
		  j++;
		  i++;
	  }
	  if(j == 2)
		{
			position = i + 2;
			break;
		}
  }
  Token = line.erase(0,position);
  /////////////////////////////

  /*Post = "__RequestVerificationToken=" + Token + "&UserName=rprojection&Password=projection&RememberMe=false";
  //"__RequestVerificationToken=qu1NjswEWXXh7LhaRfiTGPUcwOn0hYUDyRYJ57Uqkvti6HRBjRHKDERS4l0bHXIdhOuFKXrvbWN9zZ4W1bxldT12QA%2BrHhxgOEPx2Z3bXy%2BKo7C1uaqetwKSz9XTZuxapqvAobdznl4LZnucn4ktLM8%2Bss8%3D&ReturnUrl=%2F&UserName=rprojection&Password=projection&RememberMe=false"
  Postc = new char [Post.size()+1];
  strcpy (Postc, Post.c_str());
  cout << Post;
  
  char *test;
  test = curl_easy_escape(curlEncode, *Postc, 0);*/
  //////////////////////////////////
  curl4 = curl_easy_init();
  if(curl4)
  {
	curl_easy_setopt(curl4, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
	curl_easy_setopt(curl4, CURLOPT_URL, "https://ca.songselect.com/account/login?ReturnUrl=%2F");
	curl_easy_setopt(curl4, CURLOPT_SSL_VERIFYPEER, 0L);
	//curl_easy_setopt(curl4, CURLOPT_FOLLOWLOCATION, 1);
	char* Tokenc = curl_easy_escape(curl4, Token.c_str(), 0);
	Token.assign(Tokenc);
	Post = "__RequestVerificationToken=" + Token + "&UserName=rprojection&Password=projection&RememberMe=false";
	Postc = new char [Post.size()+1];
	strcpy (Postc, Post.c_str());
	void curl_free( char * Tokenc );
	curl_easy_setopt(curl4, CURLOPT_REFERER, "https://ca.songselect.com");
	curl_easy_setopt(curl4, CURLOPT_POST, 1);
	curl_easy_setopt(curl4, CURLOPT_FOLLOWLOCATION, 1);	
	curl_easy_setopt(curl4, CURLOPT_POSTFIELDS, Postc);
	curl_easy_setopt(curl4, CURLOPT_COOKIEFILE, "cookie.txt");
	curl_easy_setopt(curl4, CURLOPT_COOKIEJAR, "cookie3.txt");
    res = curl_easy_perform(curl4);
	//cout << res;
	//always cleanup 
    curl_easy_cleanup(curl4);
  }


 curl5 = curl_easy_init();
  if(curl5)
  {
	curl_easy_setopt(curl5, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
	curl_easy_setopt(curl5, CURLOPT_URL, "https://ca.songselect.com/songs/4348399/how-great-is-our-god/lyrics/download");
	curl_easy_setopt(curl5, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl5, CURLOPT_FOLLOWLOCATION, 1);	
	curl_easy_setopt(curl5, CURLOPT_REFERER, "https://ca.songselect.com/account/login");
	//curl_easy_setopt(curl5, CURLOPT_POST, 1);
	//curl_easy_setopt(curl5, CURLOPT_POSTFIELDS, "UserName=rprojection&Password=projection&RememberMe=false");
	curl_easy_setopt(curl5, CURLOPT_COOKIEFILE, "cookie3.txt");
    res = curl_easy_perform(curl5);
	cout << res;
	//always cleanup 
    curl_easy_cleanup(curl5);
  }


  system("PAUSE");

	return 0;
}

