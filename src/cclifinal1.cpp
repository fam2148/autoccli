					/* START OF PROGRAM */
///////////////////////////////////////////////////////////////////////////////
// FILE:		AutoCCLI.CPP 
// AUTHOR:		Francis MARCOGLIESE 
// DATE:		8/9/2012 
// DESCRIPTION: This program logs in to CCLI and downloads a list of songs based
//				on a the CCLI numbers in a text document.
///////////////////////////////////////////////////////////////////////////////

//v1.1 changed porject properties c/c++ code generation run time libraries to mtd isntead of mtt
//v1.2 changed file download type to usr to correct OpenLP issues
//v1.3 changed position of title search in page output after website change, removed excess data writing in console, used find functions for flexibilty

#include <iostream>	// For use of de cin and cout
#include <string>	// For use of strings
#include <fstream>	// For use of text files
#include <curl.h>	// To use cURL
#include <sstream>	// To use sstream

using namespace std;


size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata) // Function allows HTML to be written to string
{
    ostringstream *streamTitle = (ostringstream*)userdata;
    size_t count = size * nmemb;
    streamTitle->write(ptr, count);
    return count;
}

/* Never writes anything, just returns the size presented */
size_t write_nothing(char *ptr, size_t size, size_t nmemb, void *userdata)
{
   return size * nmemb;
}

int main (void)
{
	
			/* VARIABLES */
  CURL			*curl;
  CURLcode		res;
  ostringstream streamTitle;
  bool			criterion = true,
				continuer = true;
  int			i = 0,
				j = 0,
				l = 1,
				songNumber = 0,
				position = 0;
  string		slash,
				output,
				title,
				URL,
				Post,
				sourceName,
				temp,
				line,
				letter1,
				letter2,
				Token,
				urlNoTitle,
				ccliNumber;
  char			*URLc,
				*Postc,
				*Tokenc,
				*urlNoTitlec;

			/* INSTRUCTIONS */

  // Program startup

  cout << "Welcome to AutoCCLI" << endl;
	
  // Logon to SongSelect

  // Grab initial cookies
  curl = curl_easy_init();
  if(curl)
  {
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
	curl_easy_setopt(curl, CURLOPT_URL, "https://ca.songselect.com/account/login?ReturnUrl=%2F");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_REFERER, "https://ca.songselect.com");
	curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookie.txt");
	curl_easy_setopt(curl, CURLOPT_NOBODY, 1); 
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }

  // Grab CSRF token from cookie
  ifstream	cookie;
  cookie.open("cookie.txt");

  // Gets the line with the token
  for(int i=0; i<6; i++)
  {
	  getline(cookie, line, '\n');
  };
  

  // Find Token
  size_t tokenID;
  tokenID = line.find("Lw__");
  Token = line.erase(0,tokenID + 5);

  /*
  // Parses the token line for the actual token based on the double underscores
  for(i=0; i<line.size(); i++)
  {
	  letter1 = line[i];
	  letter2 = line[i+1];
	  if(letter1 == "_" & letter2 == "_")
	  {
		  j++;
		  i++;
	  }
	  if(j == 2)
		{
			position = i + 2;
			break;
		}
  }
  
  // Cuts the line to the token only
  Token = line.erase(0,position);
  */

  // Logon with POST data
  curl = curl_easy_init();
  if(curl)
  {
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
	curl_easy_setopt(curl, CURLOPT_URL, "https://ca.songselect.com/account/login?ReturnUrl=%2F");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	
	char* Tokenc = curl_easy_escape(curl, Token.c_str(), 0);
	Token.assign(Tokenc);
	Post = "__RequestVerificationToken=" + Token + "&UserName=rprojection&Password=projection&RememberMe=false";
	Postc = new char [Post.size()+1];
	strcpy (Postc, Post.c_str());
	
	void curl_free( char *Tokenc );
	
	curl_easy_setopt(curl, CURLOPT_REFERER, "https://ca.songselect.com");
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_nothing);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);	
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, Postc);
	curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookie.txt");
	curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookie.txt");
    res = curl_easy_perform(curl); 
    curl_easy_cleanup(curl);
  }

  // Load text file based on user request
  cout << "What is the name of the source file?" << endl;
  cin >> sourceName;
  
  cout << "How many songs are there?" << endl;
  cin >> songNumber;
  
  ifstream sourceFile;
  sourceFile.open(sourceName + ".txt");
  
	  
  if (sourceFile.fail())
  {
		cout << "Could not open text file"; 
		l = songNumber + 1;
  }
  ccliNumber = "1" ;
  // Capture each song based on the CCLI number
  for(l = 1; l <= songNumber; l++)
  {
	  // Variable cleanup
	  ostringstream streamTitle;
	  criterion = true;
	  ccliNumber.clear();
	  title.clear();
	  output.clear();
	  i = 0;
	  URL.clear();

	  getline(sourceFile, ccliNumber, ('\n'));
	  if(ccliNumber.size() <= 1)
	  {
			cout << "No CCLI number found.";
			break;
	  }
	  urlNoTitle = "https://ca.songselect.com/songs/" + ccliNumber + '/';
	  urlNoTitlec = new char [urlNoTitle.size()+1];
	  strcpy (urlNoTitlec, urlNoTitle.c_str());
	  
	  //Go to end of line
	  /*while(continuer)
	  {
		  char temp1;
		  sourceFile >> temp1;
		  if(temp1 == '\n')
			  break;
	  }*/

	  // Grab title of song		
	  curl = curl_easy_init();
	  if(curl)
	  {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &streamTitle);
		curl_easy_setopt(curl, CURLOPT_URL, urlNoTitlec);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		res = curl_easy_perform(curl);

		output = streamTitle.str();
		// Get title from returned HTML
		
		size_t titlePos;
		titlePos = output.find(ccliNumber) + 1;
		title = output.erase(0,titlePos + ccliNumber.size());
		//output.erase(0,9889 + ccliNumber.size());
		
		while(criterion)
			{
				slash = output[i];
				if(slash == "/")
					{
						criterion = false;
					}
				else
					i++;
			}
		
		title = output.substr(0,i);
		
		curl_easy_cleanup(curl);
	  }
	  // Clear out put for more writedata
	  output.clear();

	  // Open download page and write lyrics to file
	  curl = curl_easy_init();
	  if(curl)
	  {
		//URL = urlNoTitle + title + "/inforedirect";
		URL = urlNoTitle + title + "/lyrics/downloadusr";
		URLc = new char [URL.size()+1];
		strcpy (URLc, URL.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &streamTitle);
		curl_easy_setopt(curl, CURLOPT_URL, URLc);
		curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookie.txt");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
		res = curl_easy_perform(curl);
		
		// Write data to string and remove extra CR
		output = streamTitle.str(); 
			for(int k=0; k<output.size()-1;k++)
		{
		  char temp1 = output[k];
		  char temp2 = output[k+1];
		  if( temp1 == '\r' & temp2 == '\n')
		  {
			  output.erase(k,1);
		  }
		}
		// Remove first 3 lines of HTML in output
		int CR = 0;
		for(int k=0; k<output.size()-1;k++)
		{
		  char temp1 = output[k];
		  if( temp1 == '\n' )
		  {
			  CR++;
			  if(CR == 3)
			  {
				  output.erase(0,k+1);
				  break;
			  }
		  }
		}
		// Save string to text file
		ofstream songFile;
		songFile.open(title + ".usr");
		songFile << output;
		songFile.close();

		curl_easy_cleanup(curl);
	  }
  
  }
  sourceFile.close();

  cout << "Songs succesfully downloaded" << endl;

  system("PAUSE");

  return 0;
}
